/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  basePath: '/typescript',
  experimental: {
    outputStandalone: true,
  },
}