module.exports = {
  reactStrictMode: true,
  basePath: '/javascript',
  experimental: {
    outputStandalone: true,
  },
}
